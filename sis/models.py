from django.db import models


# Create your models here.
SEMESTER = (
    ('Spring', 'SPRING'),
    ('Summer', 'SUMMER'),
    ('Fall', 'FALL')
)


class StudentInfo(models.Model):
    std_id = models.IntegerField(verbose_name="Student ID", primary_key=True, unique=True)
    std_name = models.CharField(max_length=30, verbose_name="Student Name")
    email = models.EmailField(blank=True)
    batch = models.CharField(max_length=10, blank=True)
    cgpa = models.DecimalField(max_digits=5, decimal_places=2)
    date_of_admission = models.DateField(auto_created=True)

    def __str__(self):
        return u'%s(%s)' % (self.std_id, self.std_name)


class Subject(models.Model):
    name = models.CharField(max_length=50)
    sub_code = models.CharField(max_length=20, verbose_name="Subject Code")
    credit_hour = models.DecimalField(max_digits=3, decimal_places=1)

    def __str__(self):
        return u'%s' % self.name


class Semester(models.Model):
    std_id = models.ForeignKey(StudentInfo)
    semester = models.CharField(max_length=10, choices=SEMESTER)
    sgpa = models.DecimalField(max_digits=5, decimal_places=2)
    running_credit = models.DecimalField(max_digits=3, decimal_places=1)
    subject_list = models.ManyToManyField(to=Subject)

    def __str__(self):
        return u'%s' % self.std_id

