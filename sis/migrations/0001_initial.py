# -*- coding: utf-8 -*-
# Generated by Django 1.9.7 on 2016-07-10 18:33
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Semester',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('semester', models.CharField(choices=[('Spring', 'SPRING'), ('Summer', 'SUMMER'), ('Fall', 'FALL')], max_length=10)),
                ('sgpa', models.DecimalField(decimal_places=2, max_digits=5)),
                ('running_credit', models.DecimalField(decimal_places=1, max_digits=3)),
            ],
        ),
        migrations.CreateModel(
            name='StudentInfo',
            fields=[
                ('date_of_addmission', models.DateField(auto_created=True)),
                ('std_id', models.IntegerField(primary_key=True, serialize=False, unique=True, verbose_name='Student ID')),
                ('std_name', models.CharField(max_length=30, verbose_name='Student Name')),
                ('email', models.EmailField(blank=True, max_length=254)),
                ('batch', models.CharField(blank=True, max_length=10)),
                ('cgpa', models.DecimalField(decimal_places=2, max_digits=5)),
            ],
        ),
        migrations.CreateModel(
            name='Subject',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=50)),
                ('sub_code', models.CharField(max_length=20, verbose_name='Subject Code')),
                ('credit_hour', models.DecimalField(decimal_places=1, max_digits=3)),
            ],
        ),
        migrations.AddField(
            model_name='semester',
            name='std_id',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='sis.StudentInfo'),
        ),
        migrations.AddField(
            model_name='semester',
            name='subject_list',
            field=models.ManyToManyField(to='sis.Subject'),
        ),
    ]
