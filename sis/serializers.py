from rest_framework import serializers

from .models import StudentInfo, Subject, Semester

class StudentInfoSerializer(serializers.ModelSerializer):
    class Meta:
        model = StudentInfo
        fields = (
            'std_id',
            'std_name',
            'email',
            'batch',
            'cgpa',
            'date_of_admission'
        )

class SubjectSerializer(serializers.ModelSerializer):
    class Meta:
        model = Subject
        fields = (
            'id',
            'name',
            'sub_code',
            'credit_hour'
        )

class SemesterSerializer(serializers.ModelSerializer):
    class Meta:
        model = Semester
        fields = (
            'id',
            'std_id',
            'semester',
            'sgpa',
            'running_credit',
            'subject_list'
        )