from django.contrib import admin

# Register your models here.
from .models import StudentInfo, Semester, Subject

admin.site.register(StudentInfo)

admin.site.register(Semester)

admin.site.register(Subject)