from django.shortcuts import render

# Create your views here.
from .models import StudentInfo, Subject, Semester
from .serializers import StudentInfoSerializer, SubjectSerializer, SemesterSerializer

from rest_framework.viewsets import ModelViewSet


class StudentInfoViewSet(ModelViewSet):
    queryset = StudentInfo.objects.all()
    serializer_class = StudentInfoSerializer


class SubjectViewSet(ModelViewSet):
    queryset = Subject.objects.all()
    serializer_class = SubjectSerializer


class SemesterViewSet(ModelViewSet):
    queryset = Semester.objects.all()
    serializer_class = SemesterSerializer